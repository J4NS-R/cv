# Jans George Rautenbach's CV

Based on [AltaCV](https://www.overleaf.com/latex/templates/altacv-template/trgqjpwnmtgv).

## Requirements

- xelatex (`texlive-xetex`)
- bibtex (`texlive-bibtex-extra`)
- fontawesome (`texlive-fonts-extra`)

## Compilation

```sh
xelatex main.tex
```

